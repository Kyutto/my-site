<?php 

namespace Pimcore\Model\DataObject\Gallery;

use Pimcore\Model\DataObject;

/**
 * @method DataObject\Gallery current()
 */

class Listing extends DataObject\Listing\Concrete {

public $classId = 1;
public $className = "Gallery";


}
