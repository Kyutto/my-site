<?php

/* PimcoreCoreBundle:Profiler:targeting_data_collector.html.twig */
class __TwigTemplate_e4a77a5dcdb0e55d7f5fe300919765b0b65402e611f8f36e0b5740c527e81559 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("WebProfilerBundle:Profiler:layout.html.twig", "PimcoreCoreBundle:Profiler:targeting_data_collector.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "WebProfilerBundle:Profiler:layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6d3a9804aa06562028829c9ba629d511f092fd1b7dc8c81538d6a4c18e12ed6c = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6d3a9804aa06562028829c9ba629d511f092fd1b7dc8c81538d6a4c18e12ed6c->enter($__internal_6d3a9804aa06562028829c9ba629d511f092fd1b7dc8c81538d6a4c18e12ed6c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PimcoreCoreBundle:Profiler:targeting_data_collector.html.twig"));

        $__internal_0567b193e5240d8e6cfd19328bdc01592ea65e925e9f549ea141efbbc0d7ac01 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0567b193e5240d8e6cfd19328bdc01592ea65e925e9f549ea141efbbc0d7ac01->enter($__internal_0567b193e5240d8e6cfd19328bdc01592ea65e925e9f549ea141efbbc0d7ac01_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PimcoreCoreBundle:Profiler:targeting_data_collector.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6d3a9804aa06562028829c9ba629d511f092fd1b7dc8c81538d6a4c18e12ed6c->leave($__internal_6d3a9804aa06562028829c9ba629d511f092fd1b7dc8c81538d6a4c18e12ed6c_prof);

        
        $__internal_0567b193e5240d8e6cfd19328bdc01592ea65e925e9f549ea141efbbc0d7ac01->leave($__internal_0567b193e5240d8e6cfd19328bdc01592ea65e925e9f549ea141efbbc0d7ac01_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_935e0858015edb95ae1a6ceaa916caa8741b98c6c1f7223d3d4fc12ca74bb58a = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_935e0858015edb95ae1a6ceaa916caa8741b98c6c1f7223d3d4fc12ca74bb58a->enter($__internal_935e0858015edb95ae1a6ceaa916caa8741b98c6c1f7223d3d4fc12ca74bb58a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_75a80035f561597d504e0d5140a6f7924dd91ddda239e9fed544bf4f73ba57a8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_75a80035f561597d504e0d5140a6f7924dd91ddda239e9fed544bf4f73ba57a8->enter($__internal_75a80035f561597d504e0d5140a6f7924dd91ddda239e9fed544bf4f73ba57a8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        // line 4
        echo "    ";
        if (twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 4, $this->getSourceContext()); })()), "hasData", array())) {
            // line 5
            echo "        ";
            ob_start();
            // line 6
            echo "            ";
            echo twig_include($this->env, $context, "PimcoreCoreBundle:Profiler:target.svg.twig");
            echo "

            ";
            // line 8
            if ( !(null === twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 8, $this->getSourceContext()); })()), "documentTargetGroup", array()))) {
                // line 9
                echo "                <span class=\"sf-toolbar-value\">";
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 9, $this->getSourceContext()); })()), "documentTargetGroup", array()), "name", array()), "html", null, true);
                echo "</span>
            ";
            }
            // line 11
            echo "        ";
            $context["icon"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
            // line 12
            echo "
        ";
            // line 13
            ob_start();
            // line 14
            echo "            <div class=\"sf-toolbar-info-group\">
                ";
            // line 15
            if ( !(null === twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 15, $this->getSourceContext()); })()), "documentTargetGroup", array()))) {
                // line 16
                echo "                    <div class=\"sf-toolbar-info-piece\">
                        <b>Document Target Group</b>
                        <span>";
                // line 18
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 18, $this->getSourceContext()); })()), "documentTargetGroup", array()), "name", array()), "html", null, true);
                echo "</span>
                    </div>
                ";
            }
            // line 21
            echo "
                <div class=\"sf-toolbar-info-piece\">
                    <b>Matched Rules</b>
                    <span class=\"sf-toolbar-status\">";
            // line 24
            echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 24, $this->getSourceContext()); })()), "rules", array())), "html", null, true);
            echo "</span>
                </div>

                ";
            // line 27
            if ( !twig_test_empty(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 27, $this->getSourceContext()); })()), "targetGroups", array()))) {
                // line 28
                echo "                    <div class=\"sf-toolbar-info-piece\">
                        <h5 style=\"display: table-caption; margin-bottom: 5px; font-size: 13px\">Target Groups</h5>
                    </div>

                    ";
                // line 32
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 32, $this->getSourceContext()); })()), "targetGroups", array()));
                foreach ($context['_seq'] as $context["_key"] => $context["targetGroup"]) {
                    // line 33
                    echo "                        <div class=\"sf-toolbar-info-piece\">
                            <b>";
                    // line 34
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["targetGroup"], "name", array()), "html", null, true);
                    echo "</b>
                            <span class=\"sf-toolbar-status\">";
                    // line 35
                    echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["targetGroup"], "count", array()), "html", null, true);
                    echo "</span>
                        </div>
                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['targetGroup'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 38
                echo "                ";
            } else {
                // line 39
                echo "                    <div class=\"sf-toolbar-info-piece\">
                        <b>Target Groups</b>
                        <span class=\"sf-toolbar-status\">0</span>
                    </div>
                ";
            }
            // line 44
            echo "            </div>
        ";
            $context["text"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
            // line 46
            echo "
        ";
            // line 47
            echo twig_include($this->env, $context, "@WebProfiler/Profiler/toolbar_item.html.twig", array("link" => true));
            echo "
    ";
        }
        
        $__internal_75a80035f561597d504e0d5140a6f7924dd91ddda239e9fed544bf4f73ba57a8->leave($__internal_75a80035f561597d504e0d5140a6f7924dd91ddda239e9fed544bf4f73ba57a8_prof);

        
        $__internal_935e0858015edb95ae1a6ceaa916caa8741b98c6c1f7223d3d4fc12ca74bb58a->leave($__internal_935e0858015edb95ae1a6ceaa916caa8741b98c6c1f7223d3d4fc12ca74bb58a_prof);

    }

    // line 51
    public function block_menu($context, array $blocks = array())
    {
        $__internal_86f14bc69e1f8d7b79d5b897bfda5ead20b173edaec097c3806c754f59898cc3 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_86f14bc69e1f8d7b79d5b897bfda5ead20b173edaec097c3806c754f59898cc3->enter($__internal_86f14bc69e1f8d7b79d5b897bfda5ead20b173edaec097c3806c754f59898cc3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_897fa42588d322feef7dcbc89adb40e74f599421d55cf891283ea22db60a9077 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_897fa42588d322feef7dcbc89adb40e74f599421d55cf891283ea22db60a9077->enter($__internal_897fa42588d322feef7dcbc89adb40e74f599421d55cf891283ea22db60a9077_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 52
        echo "    <span class=\"label ";
        echo (( !twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 52, $this->getSourceContext()); })()), "hasData", array())) ? ("disabled") : (""));
        echo "\">
        <span class=\"icon\">
            ";
        // line 54
        echo twig_include($this->env, $context, "PimcoreCoreBundle:Profiler:target.svg.twig");
        echo "
        </span>
        <strong>Targeting</strong>
    </span>
";
        
        $__internal_897fa42588d322feef7dcbc89adb40e74f599421d55cf891283ea22db60a9077->leave($__internal_897fa42588d322feef7dcbc89adb40e74f599421d55cf891283ea22db60a9077_prof);

        
        $__internal_86f14bc69e1f8d7b79d5b897bfda5ead20b173edaec097c3806c754f59898cc3->leave($__internal_86f14bc69e1f8d7b79d5b897bfda5ead20b173edaec097c3806c754f59898cc3_prof);

    }

    // line 60
    public function block_panel($context, array $blocks = array())
    {
        $__internal_7cc864a7b2778d7eaf3febd895a8e7ab9a0cba5b1ebb96ab70c18905aebef242 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_7cc864a7b2778d7eaf3febd895a8e7ab9a0cba5b1ebb96ab70c18905aebef242->enter($__internal_7cc864a7b2778d7eaf3febd895a8e7ab9a0cba5b1ebb96ab70c18905aebef242_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_8976c61be833ff37c6d20c855fc70c4b08ab85978d43ffbb36064abde60ff723 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8976c61be833ff37c6d20c855fc70c4b08ab85978d43ffbb36064abde60ff723->enter($__internal_8976c61be833ff37c6d20c855fc70c4b08ab85978d43ffbb36064abde60ff723_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 61
        echo "    <h2>Targeting</h2>

    ";
        // line 63
        if ( !twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 63, $this->getSourceContext()); })()), "hasData", array())) {
            // line 64
            echo "        <div class=\"empty\">
            <p>No targeting data available.</p>
        </div>
    ";
        } else {
            // line 68
            echo "
        <div class=\"metrics\" style=\"margin-bottom: 25px\">
            ";
            // line 70
            if ( !(null === twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 70, $this->getSourceContext()); })()), "documentTargetGroup", array()))) {
                // line 71
                echo "                <div class=\"metric\">
                    <span class=\"value\">";
                // line 72
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 72, $this->getSourceContext()); })()), "documentTargetGroup", array()), "name", array()), "html", null, true);
                echo "</span>
                    <span class=\"label\">Document Target Group</span>
                </div>
            ";
            }
            // line 76
            echo "
            <div class=\"metric\">
                <span class=\"value\">";
            // line 78
            echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 78, $this->getSourceContext()); })()), "rules", array())), "html", null, true);
            echo "</span>
                <span class=\"label\">Matched Rules</span>
            </div>

            <div class=\"metric\">
                <span class=\"value\">";
            // line 83
            echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 83, $this->getSourceContext()); })()), "targetGroups", array())), "html", null, true);
            echo "</span>
                <span class=\"label\">Assigned Target Groups</span>
            </div>
        </div>

        <div class=\"sf-tabs\">
            <div class=\"tab\">
                <h3 class=\"tab-title\">Results</h3>

                <div class=\"tab-content\">
                    <h3>Matched Targeting Rules</h3>

                    <table class=\"";
            // line 95
            echo twig_escape_filter($this->env, ((array_key_exists("class", $context)) ? (_twig_default_filter((isset($context["class"]) || array_key_exists("class", $context) ? $context["class"] : (function () { throw new Twig_Error_Runtime('Variable "class" does not exist.', 95, $this->getSourceContext()); })()), "")) : ("")), "html", null, true);
            echo "\">
                        <thead>
                        <tr>
                            <th scope=\"col\" class=\"key\">ID</th>
                            <th scope=\"col\">Name</th>
                            <th scope=\"col\">Duration</th>
                            <th scope=\"col\">Conditions</th>
                            <th scope=\"col\">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        ";
            // line 106
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 106, $this->getSourceContext()); })()), "rules", array()));
            $context['_iterated'] = false;
            foreach ($context['_seq'] as $context["_key"] => $context["rule"]) {
                // line 107
                echo "                            <tr>
                                <th>";
                // line 108
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["rule"], "id", array()), "html", null, true);
                echo "</th>
                                <td>";
                // line 109
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["rule"], "name", array()), "html", null, true);
                echo "</td>
                                <td>";
                // line 110
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["rule"], "duration", array()), "html", null, true);
                echo " ms</td>
                                <td>";
                // line 111
                echo call_user_func_array($this->env->getFunction('profiler_dump')->getCallable(), array($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["rule"], "conditions", array()), ((array_key_exists("maxDepth", $context)) ? (_twig_default_filter((isset($context["maxDepth"]) || array_key_exists("maxDepth", $context) ? $context["maxDepth"] : (function () { throw new Twig_Error_Runtime('Variable "maxDepth" does not exist.', 111, $this->getSourceContext()); })()), 0)) : (0))));
                echo "</td>
                                <td>";
                // line 112
                echo call_user_func_array($this->env->getFunction('profiler_dump')->getCallable(), array($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["rule"], "actions", array()), ((array_key_exists("maxDepth", $context)) ? (_twig_default_filter((isset($context["maxDepth"]) || array_key_exists("maxDepth", $context) ? $context["maxDepth"] : (function () { throw new Twig_Error_Runtime('Variable "maxDepth" does not exist.', 112, $this->getSourceContext()); })()), 0)) : (0))));
                echo "</td>
                            </tr>
                        ";
                $context['_iterated'] = true;
            }
            if (!$context['_iterated']) {
                // line 115
                echo "                            <tr>
                                <td colspan=\"2\">(no rules matched)</td>
                            </tr>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['rule'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 119
            echo "                        </tbody>
                    </table>

                    <h3>Assigned Target Groups</h3>

                    <table class=\"";
            // line 124
            echo twig_escape_filter($this->env, ((array_key_exists("class", $context)) ? (_twig_default_filter((isset($context["class"]) || array_key_exists("class", $context) ? $context["class"] : (function () { throw new Twig_Error_Runtime('Variable "class" does not exist.', 124, $this->getSourceContext()); })()), "")) : ("")), "html", null, true);
            echo "\">
                        <thead>
                        <tr>
                            <th scope=\"col\" class=\"key\">ID</th>
                            <th scope=\"col\">Name</th>
                            <th scope=\"col\">Threshold</th>
                            <th scope=\"col\">Assignment Count</th>
                        </tr>
                        </thead>
                        <tbody>
                        ";
            // line 134
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 134, $this->getSourceContext()); })()), "targetGroups", array()));
            $context['_iterated'] = false;
            foreach ($context['_seq'] as $context["_key"] => $context["targetGroup"]) {
                // line 135
                echo "                            <tr>
                                <th>";
                // line 136
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["targetGroup"], "id", array()), "html", null, true);
                echo "</th>
                                <td>";
                // line 137
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["targetGroup"], "name", array()), "html", null, true);
                echo "</td>
                                <td>";
                // line 138
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["targetGroup"], "threshold", array()), "html", null, true);
                echo "</td>
                                <td>";
                // line 139
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), $context["targetGroup"], "count", array()), "html", null, true);
                echo "</td>
                            </tr>
                        ";
                $context['_iterated'] = true;
            }
            if (!$context['_iterated']) {
                // line 142
                echo "                            <tr>
                                <td colspan=\"2\">(no target group assignments)</td>
                            </tr>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['targetGroup'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 146
            echo "                        </tbody>
                    </table>

                    <h3>Target Groups assigned to Documents</h3>

                    <table class=\"";
            // line 151
            echo twig_escape_filter($this->env, ((array_key_exists("class", $context)) ? (_twig_default_filter((isset($context["class"]) || array_key_exists("class", $context) ? $context["class"] : (function () { throw new Twig_Error_Runtime('Variable "class" does not exist.', 151, $this->getSourceContext()); })()), "")) : ("")), "html", null, true);
            echo "\">
                        <thead>
                        <tr>
                            <th scope=\"col\">Document ID</th>
                            <th scope=\"col\">Path</th>
                            <th scope=\"col\">Target Group ID</th>
                            <th scope=\"col\">Name</th>
                        </tr>
                        </thead>
                        <tbody>
                        ";
            // line 161
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 161, $this->getSourceContext()); })()), "documentTargetGroups", array()));
            $context['_iterated'] = false;
            foreach ($context['_seq'] as $context["_key"] => $context["assignment"]) {
                // line 162
                echo "                            <tr>
                                <th>";
                // line 163
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["assignment"], "document", array()), "id", array()), "html", null, true);
                echo "</th>
                                <td>";
                // line 164
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["assignment"], "document", array()), "path", array()), "html", null, true);
                echo "</td>
                                <th>";
                // line 165
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["assignment"], "targetGroup", array()), "id", array()), "html", null, true);
                echo "</th>
                                <td>";
                // line 166
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), $context["assignment"], "targetGroup", array()), "name", array()), "html", null, true);
                echo "</td>
                            </tr>
                        ";
                $context['_iterated'] = true;
            }
            if (!$context['_iterated']) {
                // line 169
                echo "                            <tr>
                                <td colspan=\"2\">(no target groups were assigned to documents)</td>
                            </tr>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['assignment'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 173
            echo "                        </tbody>
                    </table>
                </div>
            </div>

            <div class=\"tab\">
                <h3 class=\"tab-title\">Visitor Info</h3>

                <div class=\"tab-content\">
                    <h3>Visitor Info</h3>

                    ";
            // line 184
            echo twig_include($this->env, $context, "@PimcoreCore/Profiler/key_value_table.html.twig", array("data" => twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 184, $this->getSourceContext()); })()), "visitorInfo", array())), false);
            echo "
                </div>
            </div>

            <div class=\"tab\">
                <h3 class=\"tab-title\">Storage</h3>

                <div class=\"tab-content\">
                    <h3>Session Storage</h3>

                    ";
            // line 194
            echo twig_include($this->env, $context, "@PimcoreCore/Profiler/key_value_table.html.twig", array("data" => twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 194, $this->getSourceContext()); })()), "storage", array()), "session", array())), false);
            echo "
                </div>

                <div class=\"tab-content\">
                    <h3>Visitor Storage</h3>

                    ";
            // line 200
            echo twig_include($this->env, $context, "@PimcoreCore/Profiler/key_value_table.html.twig", array("data" => twig_get_attribute($this->env, $this->getSourceContext(), twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 200, $this->getSourceContext()); })()), "storage", array()), "visitor", array())), false);
            echo "
                </div>
            </div>
        </div>

    ";
        }
        
        $__internal_8976c61be833ff37c6d20c855fc70c4b08ab85978d43ffbb36064abde60ff723->leave($__internal_8976c61be833ff37c6d20c855fc70c4b08ab85978d43ffbb36064abde60ff723_prof);

        
        $__internal_7cc864a7b2778d7eaf3febd895a8e7ab9a0cba5b1ebb96ab70c18905aebef242->leave($__internal_7cc864a7b2778d7eaf3febd895a8e7ab9a0cba5b1ebb96ab70c18905aebef242_prof);

    }

    public function getTemplateName()
    {
        return "PimcoreCoreBundle:Profiler:targeting_data_collector.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  461 => 200,  452 => 194,  439 => 184,  426 => 173,  417 => 169,  409 => 166,  405 => 165,  401 => 164,  397 => 163,  394 => 162,  389 => 161,  376 => 151,  369 => 146,  360 => 142,  352 => 139,  348 => 138,  344 => 137,  340 => 136,  337 => 135,  332 => 134,  319 => 124,  312 => 119,  303 => 115,  295 => 112,  291 => 111,  287 => 110,  283 => 109,  279 => 108,  276 => 107,  271 => 106,  257 => 95,  242 => 83,  234 => 78,  230 => 76,  223 => 72,  220 => 71,  218 => 70,  214 => 68,  208 => 64,  206 => 63,  202 => 61,  193 => 60,  178 => 54,  172 => 52,  163 => 51,  150 => 47,  147 => 46,  143 => 44,  136 => 39,  133 => 38,  124 => 35,  120 => 34,  117 => 33,  113 => 32,  107 => 28,  105 => 27,  99 => 24,  94 => 21,  88 => 18,  84 => 16,  82 => 15,  79 => 14,  77 => 13,  74 => 12,  71 => 11,  65 => 9,  63 => 8,  57 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'WebProfilerBundle:Profiler:layout.html.twig' %}

{% block toolbar %}
    {% if collector.hasData %}
        {% set icon %}
            {{ include(\"PimcoreCoreBundle:Profiler:target.svg.twig\") }}

            {% if collector.documentTargetGroup is not null %}
                <span class=\"sf-toolbar-value\">{{ collector.documentTargetGroup.name }}</span>
            {% endif %}
        {% endset %}

        {% set text %}
            <div class=\"sf-toolbar-info-group\">
                {% if collector.documentTargetGroup is not null %}
                    <div class=\"sf-toolbar-info-piece\">
                        <b>Document Target Group</b>
                        <span>{{ collector.documentTargetGroup.name }}</span>
                    </div>
                {% endif %}

                <div class=\"sf-toolbar-info-piece\">
                    <b>Matched Rules</b>
                    <span class=\"sf-toolbar-status\">{{ collector.rules|length }}</span>
                </div>

                {% if collector.targetGroups is not empty %}
                    <div class=\"sf-toolbar-info-piece\">
                        <h5 style=\"display: table-caption; margin-bottom: 5px; font-size: 13px\">Target Groups</h5>
                    </div>

                    {% for targetGroup in collector.targetGroups %}
                        <div class=\"sf-toolbar-info-piece\">
                            <b>{{ targetGroup.name }}</b>
                            <span class=\"sf-toolbar-status\">{{ targetGroup.count }}</span>
                        </div>
                    {% endfor %}
                {% else %}
                    <div class=\"sf-toolbar-info-piece\">
                        <b>Target Groups</b>
                        <span class=\"sf-toolbar-status\">0</span>
                    </div>
                {% endif %}
            </div>
        {% endset %}

        {{ include('@WebProfiler/Profiler/toolbar_item.html.twig', { link: true }) }}
    {% endif %}
{% endblock %}

{% block menu %}
    <span class=\"label {{ (not collector.hasData) ? 'disabled' }}\">
        <span class=\"icon\">
            {{ include(\"PimcoreCoreBundle:Profiler:target.svg.twig\") }}
        </span>
        <strong>Targeting</strong>
    </span>
{% endblock %}

{% block panel %}
    <h2>Targeting</h2>

    {% if not collector.hasData %}
        <div class=\"empty\">
            <p>No targeting data available.</p>
        </div>
    {% else %}

        <div class=\"metrics\" style=\"margin-bottom: 25px\">
            {% if collector.documentTargetGroup is not null %}
                <div class=\"metric\">
                    <span class=\"value\">{{ collector.documentTargetGroup.name }}</span>
                    <span class=\"label\">Document Target Group</span>
                </div>
            {% endif %}

            <div class=\"metric\">
                <span class=\"value\">{{ collector.rules|length }}</span>
                <span class=\"label\">Matched Rules</span>
            </div>

            <div class=\"metric\">
                <span class=\"value\">{{ collector.targetGroups|length }}</span>
                <span class=\"label\">Assigned Target Groups</span>
            </div>
        </div>

        <div class=\"sf-tabs\">
            <div class=\"tab\">
                <h3 class=\"tab-title\">Results</h3>

                <div class=\"tab-content\">
                    <h3>Matched Targeting Rules</h3>

                    <table class=\"{{ class|default('') }}\">
                        <thead>
                        <tr>
                            <th scope=\"col\" class=\"key\">ID</th>
                            <th scope=\"col\">Name</th>
                            <th scope=\"col\">Duration</th>
                            <th scope=\"col\">Conditions</th>
                            <th scope=\"col\">Actions</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for rule in collector.rules %}
                            <tr>
                                <th>{{ rule.id }}</th>
                                <td>{{ rule.name }}</td>
                                <td>{{ rule.duration }} ms</td>
                                <td>{{ profiler_dump(rule.conditions, maxDepth=maxDepth|default(0)) }}</td>
                                <td>{{ profiler_dump(rule.actions, maxDepth=maxDepth|default(0)) }}</td>
                            </tr>
                        {% else %}
                            <tr>
                                <td colspan=\"2\">(no rules matched)</td>
                            </tr>
                        {% endfor %}
                        </tbody>
                    </table>

                    <h3>Assigned Target Groups</h3>

                    <table class=\"{{ class|default('') }}\">
                        <thead>
                        <tr>
                            <th scope=\"col\" class=\"key\">ID</th>
                            <th scope=\"col\">Name</th>
                            <th scope=\"col\">Threshold</th>
                            <th scope=\"col\">Assignment Count</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for targetGroup in collector.targetGroups %}
                            <tr>
                                <th>{{ targetGroup.id }}</th>
                                <td>{{ targetGroup.name }}</td>
                                <td>{{ targetGroup.threshold }}</td>
                                <td>{{ targetGroup.count }}</td>
                            </tr>
                        {% else %}
                            <tr>
                                <td colspan=\"2\">(no target group assignments)</td>
                            </tr>
                        {% endfor %}
                        </tbody>
                    </table>

                    <h3>Target Groups assigned to Documents</h3>

                    <table class=\"{{ class|default('') }}\">
                        <thead>
                        <tr>
                            <th scope=\"col\">Document ID</th>
                            <th scope=\"col\">Path</th>
                            <th scope=\"col\">Target Group ID</th>
                            <th scope=\"col\">Name</th>
                        </tr>
                        </thead>
                        <tbody>
                        {% for assignment in collector.documentTargetGroups %}
                            <tr>
                                <th>{{ assignment.document.id }}</th>
                                <td>{{ assignment.document.path }}</td>
                                <th>{{ assignment.targetGroup.id }}</th>
                                <td>{{ assignment.targetGroup.name }}</td>
                            </tr>
                        {% else %}
                            <tr>
                                <td colspan=\"2\">(no target groups were assigned to documents)</td>
                            </tr>
                        {% endfor %}
                        </tbody>
                    </table>
                </div>
            </div>

            <div class=\"tab\">
                <h3 class=\"tab-title\">Visitor Info</h3>

                <div class=\"tab-content\">
                    <h3>Visitor Info</h3>

                    {{ include('@PimcoreCore/Profiler/key_value_table.html.twig', { data: collector.visitorInfo }, with_context = false) }}
                </div>
            </div>

            <div class=\"tab\">
                <h3 class=\"tab-title\">Storage</h3>

                <div class=\"tab-content\">
                    <h3>Session Storage</h3>

                    {{ include('@PimcoreCore/Profiler/key_value_table.html.twig', { data: collector.storage.session }, with_context = false) }}
                </div>

                <div class=\"tab-content\">
                    <h3>Visitor Storage</h3>

                    {{ include('@PimcoreCore/Profiler/key_value_table.html.twig', { data: collector.storage.visitor }, with_context = false) }}
                </div>
            </div>
        </div>

    {% endif %}
{% endblock %}
", "PimcoreCoreBundle:Profiler:targeting_data_collector.html.twig", "D:\\PROJECT\\SIMPLY PROJECT\\pimcore\\lib\\Pimcore\\Bundle\\CoreBundle/Resources/views/Profiler/targeting_data_collector.html.twig");
    }
}
