<?php

/* @Twig/images/chevron-right.svg */
class __TwigTemplate_6deaed0294a8ffb912438c14dbaa277cf26f4e728b0b62e9baf5af02e849fccb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8a3ecd41a2574ce35a32329efd3edef853829cdafbbc13e9be71fa4cd3c0be1d = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_8a3ecd41a2574ce35a32329efd3edef853829cdafbbc13e9be71fa4cd3c0be1d->enter($__internal_8a3ecd41a2574ce35a32329efd3edef853829cdafbbc13e9be71fa4cd3c0be1d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/chevron-right.svg"));

        $__internal_b75953bbedbeee87ff08cd49f24a6d30fdb22873cefb742cf8c8ff3ab796d949 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b75953bbedbeee87ff08cd49f24a6d30fdb22873cefb742cf8c8ff3ab796d949->enter($__internal_b75953bbedbeee87ff08cd49f24a6d30fdb22873cefb742cf8c8ff3ab796d949_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/images/chevron-right.svg"));

        // line 1
        echo "<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M1363 877l-742 742q-19 19-45 19t-45-19l-166-166q-19-19-19-45t19-45l531-531-531-531q-19-19-19-45t19-45l166-166q19-19 45-19t45 19l742 742q19 19 19 45t-19 45z\"/></svg>
";
        
        $__internal_8a3ecd41a2574ce35a32329efd3edef853829cdafbbc13e9be71fa4cd3c0be1d->leave($__internal_8a3ecd41a2574ce35a32329efd3edef853829cdafbbc13e9be71fa4cd3c0be1d_prof);

        
        $__internal_b75953bbedbeee87ff08cd49f24a6d30fdb22873cefb742cf8c8ff3ab796d949->leave($__internal_b75953bbedbeee87ff08cd49f24a6d30fdb22873cefb742cf8c8ff3ab796d949_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/images/chevron-right.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg width=\"1792\" height=\"1792\" viewBox=\"0 0 1792 1792\" xmlns=\"http://www.w3.org/2000/svg\"><path fill=\"#FFF\" d=\"M1363 877l-742 742q-19 19-45 19t-45-19l-166-166q-19-19-19-45t19-45l531-531-531-531q-19-19-19-45t19-45l166-166q19-19 45-19t45 19l742 742q19 19 19 45t-19 45z\"/></svg>
", "@Twig/images/chevron-right.svg", "D:\\PROJECT\\SIMPLY PROJECT\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\images\\chevron-right.svg");
    }
}
