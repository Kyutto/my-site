<?php

/* PimcoreCoreBundle:Profiler:data_collector.html.twig */
class __TwigTemplate_73d46be2804d249715a10030a10a3f17b52e269009ea7173d4023dbbb181ddd3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("WebProfilerBundle:Profiler:layout.html.twig", "PimcoreCoreBundle:Profiler:data_collector.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "WebProfilerBundle:Profiler:layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_6af6094ce1f29ae2757959fce12408074376ba4bfd9f326f93669e8c92b07dee = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6af6094ce1f29ae2757959fce12408074376ba4bfd9f326f93669e8c92b07dee->enter($__internal_6af6094ce1f29ae2757959fce12408074376ba4bfd9f326f93669e8c92b07dee_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PimcoreCoreBundle:Profiler:data_collector.html.twig"));

        $__internal_c77c87b94d9e095c5f1aeae8c51735ce5b52a523f623c40408953ab8277d47bc = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c77c87b94d9e095c5f1aeae8c51735ce5b52a523f623c40408953ab8277d47bc->enter($__internal_c77c87b94d9e095c5f1aeae8c51735ce5b52a523f623c40408953ab8277d47bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "PimcoreCoreBundle:Profiler:data_collector.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_6af6094ce1f29ae2757959fce12408074376ba4bfd9f326f93669e8c92b07dee->leave($__internal_6af6094ce1f29ae2757959fce12408074376ba4bfd9f326f93669e8c92b07dee_prof);

        
        $__internal_c77c87b94d9e095c5f1aeae8c51735ce5b52a523f623c40408953ab8277d47bc->leave($__internal_c77c87b94d9e095c5f1aeae8c51735ce5b52a523f623c40408953ab8277d47bc_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_b43c3cdace74f92685c4c06e23afdb00b198fd249647ef1cfa748dc2f994ed68 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_b43c3cdace74f92685c4c06e23afdb00b198fd249647ef1cfa748dc2f994ed68->enter($__internal_b43c3cdace74f92685c4c06e23afdb00b198fd249647ef1cfa748dc2f994ed68_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        $__internal_76443aa05020de0cdf4c2d4c920c83f1b8bd5779029c49ab02fffbd57e4c915e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_76443aa05020de0cdf4c2d4c920c83f1b8bd5779029c49ab02fffbd57e4c915e->enter($__internal_76443aa05020de0cdf4c2d4c920c83f1b8bd5779029c49ab02fffbd57e4c915e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        // line 4
        echo "    ";
        ob_start();
        // line 5
        echo "        ";
        // line 6
        echo "        <div style=\"padding-top: 3px\">
            ";
        // line 7
        echo twig_include($this->env, $context, "PimcoreCoreBundle:Profiler:logo.svg.twig");
        echo "
        </div>
    ";
        $context["icon"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 10
        echo "
    ";
        // line 11
        ob_start();
        // line 12
        echo "        ";
        // line 14
        echo "        <div class=\"sf-toolbar-info-piece\">
            <b>Version</b>
            <span>";
        // line 16
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 16, $this->getSourceContext()); })()), "version", array()), "html", null, true);
        echo " build ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 16, $this->getSourceContext()); })()), "revision", array()), "html", null, true);
        echo "</span>
        </div>

        <div class=\"sf-toolbar-info-piece\">
            <b>Debug Mode</b>
            <span>";
        // line 21
        echo ((twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 21, $this->getSourceContext()); })()), "debugMode", array())) ? ("enabled") : ("disabled"));
        echo "</span>
        </div>

        <div class=\"sf-toolbar-info-piece\">
            <b>Dev Mode</b>
            <span>";
        // line 26
        echo ((twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 26, $this->getSourceContext()); })()), "devMode", array())) ? ("enabled") : ("disabled"));
        echo "</span>
        </div>

        <div class=\"sf-toolbar-info-piece\">
            <b>Context</b>
            <span>";
        // line 31
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 31, $this->getSourceContext()); })()), "context", array()), "html", null, true);
        echo "</span>
        </div>
    ";
        $context["text"] = ('' === $tmp = ob_get_clean()) ? '' : new Twig_Markup($tmp, $this->env->getCharset());
        // line 34
        echo "
    ";
        // line 37
        echo "    ";
        echo twig_include($this->env, $context, "@WebProfiler/Profiler/toolbar_item.html.twig", array("link" => true));
        echo "
";
        
        $__internal_76443aa05020de0cdf4c2d4c920c83f1b8bd5779029c49ab02fffbd57e4c915e->leave($__internal_76443aa05020de0cdf4c2d4c920c83f1b8bd5779029c49ab02fffbd57e4c915e_prof);

        
        $__internal_b43c3cdace74f92685c4c06e23afdb00b198fd249647ef1cfa748dc2f994ed68->leave($__internal_b43c3cdace74f92685c4c06e23afdb00b198fd249647ef1cfa748dc2f994ed68_prof);

    }

    // line 40
    public function block_menu($context, array $blocks = array())
    {
        $__internal_6e0504cba5762dee9475244e927909600e07ee13c183e76dac961f30b13aaf33 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_6e0504cba5762dee9475244e927909600e07ee13c183e76dac961f30b13aaf33->enter($__internal_6e0504cba5762dee9475244e927909600e07ee13c183e76dac961f30b13aaf33_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        $__internal_a2ec6acc3bb6e3af732ccd933f783a44ae929277bd19ee7717b606cf1446224d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a2ec6acc3bb6e3af732ccd933f783a44ae929277bd19ee7717b606cf1446224d->enter($__internal_a2ec6acc3bb6e3af732ccd933f783a44ae929277bd19ee7717b606cf1446224d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 41
        echo "    <span class=\"label\">
        <span class=\"icon\">
            ";
        // line 43
        echo twig_include($this->env, $context, "PimcoreCoreBundle:Profiler:logo.svg.twig");
        echo "
        </span>
        <strong>Pimcore</strong>
    </span>
";
        
        $__internal_a2ec6acc3bb6e3af732ccd933f783a44ae929277bd19ee7717b606cf1446224d->leave($__internal_a2ec6acc3bb6e3af732ccd933f783a44ae929277bd19ee7717b606cf1446224d_prof);

        
        $__internal_6e0504cba5762dee9475244e927909600e07ee13c183e76dac961f30b13aaf33->leave($__internal_6e0504cba5762dee9475244e927909600e07ee13c183e76dac961f30b13aaf33_prof);

    }

    // line 49
    public function block_panel($context, array $blocks = array())
    {
        $__internal_ceeee4e296e88653cfa076d242768b82af8be826f5f2d356521bd8082d9aea88 = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_ceeee4e296e88653cfa076d242768b82af8be826f5f2d356521bd8082d9aea88->enter($__internal_ceeee4e296e88653cfa076d242768b82af8be826f5f2d356521bd8082d9aea88_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        $__internal_ce0e8044f6834aca7f3bffaec578020c1b5b1a223098d50526e9bd557227322a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ce0e8044f6834aca7f3bffaec578020c1b5b1a223098d50526e9bd557227322a->enter($__internal_ce0e8044f6834aca7f3bffaec578020c1b5b1a223098d50526e9bd557227322a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 50
        echo "    <h2>Pimcore</h2>

    <div class=\"metrics\">
        <div class=\"metric\">
            <span class=\"value\">";
        // line 54
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 54, $this->getSourceContext()); })()), "version", array()), "html", null, true);
        echo "</span>
            <span class=\"label\">Version</span>
        </div>

        <div class=\"metric\">
            <span class=\"value\">";
        // line 59
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 59, $this->getSourceContext()); })()), "revision", array()), "html", null, true);
        echo "</span>
            <span class=\"label\">Build</span>
        </div>

        <div class=\"metric\">
            <span class=\"value\">";
        // line 64
        echo ((twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 64, $this->getSourceContext()); })()), "debugMode", array())) ? ("enabled") : ("disabled"));
        echo "</span>
            <span class=\"label\">Debug Mode</span>
        </div>

        <div class=\"metric\">
            <span class=\"value\">";
        // line 69
        echo ((twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 69, $this->getSourceContext()); })()), "devMode", array())) ? ("enabled") : ("disabled"));
        echo "</span>
            <span class=\"label\">Dev Mode</span>
        </div>

        <div class=\"metric\">
            <span class=\"value\">";
        // line 74
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->getSourceContext(), (isset($context["collector"]) || array_key_exists("collector", $context) ? $context["collector"] : (function () { throw new Twig_Error_Runtime('Variable "collector" does not exist.', 74, $this->getSourceContext()); })()), "context", array()), "html", null, true);
        echo "</span>
            <span class=\"label\">Request Context</span>
        </div>
    </div>
";
        
        $__internal_ce0e8044f6834aca7f3bffaec578020c1b5b1a223098d50526e9bd557227322a->leave($__internal_ce0e8044f6834aca7f3bffaec578020c1b5b1a223098d50526e9bd557227322a_prof);

        
        $__internal_ceeee4e296e88653cfa076d242768b82af8be826f5f2d356521bd8082d9aea88->leave($__internal_ceeee4e296e88653cfa076d242768b82af8be826f5f2d356521bd8082d9aea88_prof);

    }

    public function getTemplateName()
    {
        return "PimcoreCoreBundle:Profiler:data_collector.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  199 => 74,  191 => 69,  183 => 64,  175 => 59,  167 => 54,  161 => 50,  152 => 49,  137 => 43,  133 => 41,  124 => 40,  111 => 37,  108 => 34,  102 => 31,  94 => 26,  86 => 21,  76 => 16,  72 => 14,  70 => 12,  68 => 11,  65 => 10,  59 => 7,  56 => 6,  54 => 5,  51 => 4,  42 => 3,  11 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'WebProfilerBundle:Profiler:layout.html.twig' %}

{% block toolbar %}
    {% set icon %}
        {# this is the content displayed as a panel in the toolbar #}
        <div style=\"padding-top: 3px\">
            {{ include(\"PimcoreCoreBundle:Profiler:logo.svg.twig\") }}
        </div>
    {% endset %}

    {% set text %}
        {# this is the content displayed when hovering the mouse over
           the toolbar panel #}
        <div class=\"sf-toolbar-info-piece\">
            <b>Version</b>
            <span>{{ collector.version }} build {{ collector.revision }}</span>
        </div>

        <div class=\"sf-toolbar-info-piece\">
            <b>Debug Mode</b>
            <span>{{ collector.debugMode ? 'enabled' : 'disabled' }}</span>
        </div>

        <div class=\"sf-toolbar-info-piece\">
            <b>Dev Mode</b>
            <span>{{ collector.devMode ? 'enabled' : 'disabled' }}</span>
        </div>

        <div class=\"sf-toolbar-info-piece\">
            <b>Context</b>
            <span>{{ collector.context }}</span>
        </div>
    {% endset %}

    {# the 'link' value set to 'false' means that this panel doesn't
       show a section in the web profiler #}
    {{ include('@WebProfiler/Profiler/toolbar_item.html.twig', { link: true }) }}
{% endblock %}

{% block menu %}
    <span class=\"label\">
        <span class=\"icon\">
            {{ include(\"PimcoreCoreBundle:Profiler:logo.svg.twig\") }}
        </span>
        <strong>Pimcore</strong>
    </span>
{% endblock %}

{% block panel %}
    <h2>Pimcore</h2>

    <div class=\"metrics\">
        <div class=\"metric\">
            <span class=\"value\">{{ collector.version }}</span>
            <span class=\"label\">Version</span>
        </div>

        <div class=\"metric\">
            <span class=\"value\">{{ collector.revision }}</span>
            <span class=\"label\">Build</span>
        </div>

        <div class=\"metric\">
            <span class=\"value\">{{ collector.debugMode ? 'enabled' : 'disabled' }}</span>
            <span class=\"label\">Debug Mode</span>
        </div>

        <div class=\"metric\">
            <span class=\"value\">{{ collector.devMode ? 'enabled' : 'disabled' }}</span>
            <span class=\"label\">Dev Mode</span>
        </div>

        <div class=\"metric\">
            <span class=\"value\">{{ collector.context }}</span>
            <span class=\"label\">Request Context</span>
        </div>
    </div>
{% endblock %}
", "PimcoreCoreBundle:Profiler:data_collector.html.twig", "D:\\PROJECT\\SIMPLY PROJECT\\pimcore\\lib\\Pimcore\\Bundle\\CoreBundle/Resources/views/Profiler/data_collector.html.twig");
    }
}
