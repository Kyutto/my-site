<?php

/* @Doctrine/Collector/icon.svg */
class __TwigTemplate_9848e6d262dc63d55d84d467b085a4ea62a5ac998331ff35f62ab974cd623086 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0a1a014b3ae343e00f4d40e5381517300a3d5665a985d109ed314e57c87bfb9f = $this->env->getExtension("Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension");
        $__internal_0a1a014b3ae343e00f4d40e5381517300a3d5665a985d109ed314e57c87bfb9f->enter($__internal_0a1a014b3ae343e00f4d40e5381517300a3d5665a985d109ed314e57c87bfb9f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Doctrine/Collector/icon.svg"));

        $__internal_e2d04ba349b9e0edb9b4267f2c29439ccdce3479ef1aa7f8bb9e1133c0396b3d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e2d04ba349b9e0edb9b4267f2c29439ccdce3479ef1aa7f8bb9e1133c0396b3d->enter($__internal_e2d04ba349b9e0edb9b4267f2c29439ccdce3479ef1aa7f8bb9e1133c0396b3d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Doctrine/Collector/icon.svg"));

        // line 1
        echo "<svg version=\"1.1\"xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path fill=\"#AAAAAA\" d=\"M5,8h14c1.7,0,3-1.3,3-3s-1.3-3-3-3H5C3.3,2,2,3.3,2,5S3.3,8,5,8z M18,3.6c0.8,0,1.5,0.7,1.5,1.5S18.8,6.6,18,6.6s-1.5-0.7-1.5-1.5S17.2,3.6,18,3.6z M19,9H5c-1.7,0-3,1.3-3,3s1.3,3,3,3h14c1.7,0,3-1.3,3-3S20.7,9,19,9z M18,13.6
    c-0.8,0-1.5-0.7-1.5-1.5s0.7-1.5,1.5-1.5s1.5,0.7,1.5,1.5S18.8,13.6,18,13.6z M19,16H5c-1.7,0-3,1.3-3,3s1.3,3,3,3h14c1.7,0,3-1.3,3-3S20.7,16,19,16z M18,20.6c-0.8,0-1.5-0.7-1.5-1.5s0.7-1.5,1.5-1.5s1.5,0.7,1.5,1.5S18.8,20.6,18,20.6z\"/>
</svg>
";
        
        $__internal_0a1a014b3ae343e00f4d40e5381517300a3d5665a985d109ed314e57c87bfb9f->leave($__internal_0a1a014b3ae343e00f4d40e5381517300a3d5665a985d109ed314e57c87bfb9f_prof);

        
        $__internal_e2d04ba349b9e0edb9b4267f2c29439ccdce3479ef1aa7f8bb9e1133c0396b3d->leave($__internal_e2d04ba349b9e0edb9b4267f2c29439ccdce3479ef1aa7f8bb9e1133c0396b3d_prof);

    }

    public function getTemplateName()
    {
        return "@Doctrine/Collector/icon.svg";
    }

    public function getDebugInfo()
    {
        return array (  25 => 1,);
    }

    public function getSourceContext()
    {
        return new Twig_Source("<svg version=\"1.1\"xmlns=\"http://www.w3.org/2000/svg\" x=\"0px\" y=\"0px\" width=\"24\" height=\"24\" viewBox=\"0 0 24 24\" enable-background=\"new 0 0 24 24\" xml:space=\"preserve\">
    <path fill=\"#AAAAAA\" d=\"M5,8h14c1.7,0,3-1.3,3-3s-1.3-3-3-3H5C3.3,2,2,3.3,2,5S3.3,8,5,8z M18,3.6c0.8,0,1.5,0.7,1.5,1.5S18.8,6.6,18,6.6s-1.5-0.7-1.5-1.5S17.2,3.6,18,3.6z M19,9H5c-1.7,0-3,1.3-3,3s1.3,3,3,3h14c1.7,0,3-1.3,3-3S20.7,9,19,9z M18,13.6
    c-0.8,0-1.5-0.7-1.5-1.5s0.7-1.5,1.5-1.5s1.5,0.7,1.5,1.5S18.8,13.6,18,13.6z M19,16H5c-1.7,0-3,1.3-3,3s1.3,3,3,3h14c1.7,0,3-1.3,3-3S20.7,16,19,16z M18,20.6c-0.8,0-1.5-0.7-1.5-1.5s0.7-1.5,1.5-1.5s1.5,0.7,1.5,1.5S18.8,20.6,18,20.6z\"/>
</svg>
", "@Doctrine/Collector/icon.svg", "D:\\PROJECT\\SIMPLY PROJECT\\vendor\\doctrine\\doctrine-bundle\\Resources\\views\\Collector\\icon.svg");
    }
}
