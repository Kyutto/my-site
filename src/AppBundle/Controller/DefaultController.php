<?php

namespace AppBundle\Controller;

use Pimcore\Controller\FrontendController;
use Pimcore\Model\DataObject\Gallery;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends FrontendController
{
    public function defaultAction(Request $request)
    {
        $gallery = new Gallery\Listing();
        $gallery->load();
        $return = array();
        foreach ($gallery->getObjects() as $gal) {
            $data = new \stdClass();
            $data->title = $gal->getTitle();
            $data->image = $gal->getImage()->getPath() . $gal->getImage()->getFilename();
            $imageInfo = $this->getExif($gal->getImage());
            $info = $imageInfo["exif"];
            $data->cam = $info["Make"];
            $data->cam_model = $info["Model"];
            $data->aperture = $info["ApertureValue"];
            $data->shutterspeed = $info["ShutterSpeedValue"];
            $data->iso = $info["ISOSpeedRatings"];
            $return[] = $data;
        }
        $this->view->data = $return;
    }

    function getExif($asset = null)
    {
        if ($asset) {
            if (function_exists("exif_read_data") && is_file($asset->getFileSystemPath())) {
                $supportedTypes = array(IMAGETYPE_JPEG, IMAGETYPE_TIFF_II, IMAGETYPE_TIFF_MM);

                if (in_array(exif_imagetype($asset->getFileSystemPath()), $supportedTypes)) {
                    $exif = @ exif_read_data($asset->getFileSystemPath());
                    if (is_array($exif)) {
                        $imageInfo["exif"] = array();
                        foreach ($exif as $name => $value) {
                            if ((is_string($value) && strlen($value) < 50) || is_numeric($value)) {
                                // this is to ensure that the data can be converted to json (must be utf8)
                                if (mb_check_encoding($value, "UTF-8")) {
                                    if ($name == "ShutterSpeedValue") {
                                        $data[$name] = $value;
                                        $shutter = $this->exif_get_shutter($data);
                                        $imageInfo["exif"][$name] = $shutter;
                                    } else if ($name == "ApertureValue") {
                                        $data[$name] = $value;
                                        $shutter = $this->exif_get_fstop($data);
                                        $imageInfo["exif"][$name] = $shutter;
                                    } else {
                                        $imageInfo["exif"][$name] = $value;
                                    }
                                }
                            }
                        }
                    }
                }
            }
            return $imageInfo;
        } else {
            return false;
        }
    }

    function exif_get_float($value)
    {
        $pos = strpos($value, '/');
        if ($pos === false) return (float)$value;
        $a = (float)substr($value, 0, $pos);
        $b = (float)substr($value, $pos + 1);
        return ($b == 0) ? ($a) : ($a / $b);
    }

    function exif_get_shutter(&$exif)
    {
        if (!isset($exif['ShutterSpeedValue'])) return false;
        $apex = $this->exif_get_float($exif['ShutterSpeedValue']);
        $shutter = pow(2, -$apex);
        if ($shutter == 0) return false;
        if ($shutter >= 1) return round($shutter) . 's';
        return '1/' . round(1 / $shutter) . 's';
    }

    function exif_get_fstop(&$exif)
    {
        if (!isset($exif['ApertureValue'])) return false;
        $apex = $this->exif_get_float($exif['ApertureValue']);
        $fstop = pow(2, $apex / 2);
        if ($fstop == 0) return false;
        return 'f/' . round($fstop, 1);
    }
}
