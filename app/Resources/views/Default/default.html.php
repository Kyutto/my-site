<!DOCTYPE HTML>

<html>
<head>
    <title>GALLERY</title>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <!--[if lte IE 8]>
    <script src="/assets/js/ie/html5shiv.js"></script><![endif]-->
    <link rel="stylesheet" href="/assets/css/main.css"/>
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="/assets/css/ie8.css"/><![endif]-->
    <!--[if lte IE 9]>
    <link rel="stylesheet" href="/assets/css/ie9.css"/><![endif]-->
    <noscript>
        <link rel="stylesheet" href="/assets/css/noscript.css"/>
    </noscript>
</head>
<body class="is-loading-0 is-loading-1 is-loading-2">

<!-- Main -->
<div id="main">

    <!-- Header -->
    <header id="header">
        <div class="logohead">
            <img src="/assets/images/logo.png" alt="" width="150">
        </div>
        <h1 class="text-center">Gallery</h1>
        <p class="text-center">Just my photograph gallery</p>
        <ul class="icons text-center">
            <li><a href="https://www.facebook.com/rukasudesu" target="_blank" class="icon fa-facebook"><span
                        class="label">Facebook</span></a></li>
            <li><a href="#" class="icon fa-instagram"><span class="label">Instagram</span></a></li>
            <li><a href="mailto:lukastrysetyohadi@gmail.com?subject=Gallery Photo" class="icon fa-envelope-o"><span
                        class="label">Email</span></a></li>
        </ul>
    </header>
    <!-- Thumbnail -->
    <section id="thumbnails">
        <?php
        foreach ($this->data as $data) {
            ?>
            <article>
                <a class="thumbnail" href="<?php echo $data->image; ?>" data-position="left center"><img
                        src="<?php echo $data->image; ?>" alt="<?php echo strtolower($data->title); ?>"/></a>
                <h2><?php echo $data->title; ?></h2>
                <h1>Camera: <?php echo $data->cam . " " . $data->cam_model ?></h1>
                <h1>ISO: <?php echo $data->iso ?></h1>
                <h1>Aperture: <?php echo $data->aperture ?></h1>
                <h1>Shutter Speed: <?php echo $data->shutterspeed ?></h1>
            </article>
            <?php
        }
        ?>
    </section>

    <!-- Footer -->
    <footer id="footer">
        <ul class="copyright text-center">
            <li>&copy; Click</li>
            <li>Design: <a href="http:themewagon.com">Themewagon</a></li>

        </ul>
    </footer>
</div>
<!-- Scripts -->
<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/skel.min.js"></script>
<!--[if lte IE 8]>
<script src="/assets/js/ie/respond.min.js"></script><![endif]-->
<script src="/assets/js/main.js"></script>
<script type="text/javascript">
    $('.toggle').click(function () {
        $('.caption').toggle();
    })
</script>
</body>
</html>